
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="calcul.png" type="image/icon type">
    <link rel="stylesheet" href="bootstrap.min.css">
    <title>Calculator</title>
</head>
<body>
    <div class="container">
        <form action="index.php" method="GET" class="form">
            <div>
                <label >Enter number 1:</label>
                <input type="number"  name="number1" class="form-control">
            </div>
            <div>
                <label >Enter number 2:</label>
                <input type="number"  name="number2" class="form-control">
            </div>
            <br>
            <input type="submit" class="btn btn-primary">
        </form>
        <?php if(isset($_GET['number1']) && isset($_GET['number2'])): ?>
        <br/>
        <?php echo (int)$_GET["number1"]+(int)$_GET["number2"]; ?>
        <?php endif; ?>
        
    </div>
</body>
</html>